package barley.core.multitenancy;

import javax.xml.namespace.QName;

public class __MultitenantConstants {

	public static String IS_SUPER_TENANT = "is.super.tenant";

    public static final String MULTITENANT_DISPATCHER_SERVICE = "__MultitenantDispatcherService";
    public static final QName MULTITENANT_DISPATCHER_OPERATION = new QName("dispatch");

    public static final String TENANT_MR_STARTED_FAULT = "tenantMRStartedFault";
    public static final String TENANT_DOMAIN = "tenantDomain";
    public static final String TENANT_ID = "tenantId";
    public static final String SUPER_TENANT_NAME = "super";
    public static final String TENANT_AWARE_URL_PREFIX = "t";
    //public static final int SUPER_TENANT_ID = 0;
    public static final String TENANT_DOMAIN_HEADER_NAMESPACE = "http://cloud.wso2.com/";
    public static final String TENANT_DOMAIN_HEADER_NAME = "TenantDomain";
    public static final String SUPER_TENANT_DOMAIN = "UserName.SuperTenantDomain";

    public final static String REQUIRE_SUPER_TENANT = "require-super-tenant";
    public final static String REQUIRE_NOT_SUPER_TENANT = "require-not-super-tenant";
    public static String IS_MASTER_TENANT = "is-master-tenant";

    public static final String TENANT_MODULE_BUNDLES = "tenant.module.bundles";
    public static final String TENANT_MODULE_LOADED = "tenant.module.loaded";

    /**
     * Last time at which this tenant was accessed
     */
    public static final String LAST_ACCESSED = "last.accessed.time";

    /**
     * The allowed tenant idle time before the tenant code is undeployed
     */
    public static final String TENANT_IDLE_TIME = "tenant.idle.time";

    public static final String TRANSPORT_OUT_DESCRIPTION = "TRANSPORT_OUT_DESCRIPTION";
    public static final String TENANT_REQUEST_MSG_CTX = "TENANT_REQUEST_MSG_CTX";

    public static final String SSO_AUTH_SESSION_ID = "SSOAuthSessionID";
    
    public static final String SUPER_TENANT_DOMAIN_NAME = "carbon.super";
    
}
