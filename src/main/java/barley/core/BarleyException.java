package barley.core;

public class BarleyException extends Exception {
    public BarleyException() {
    }

    public BarleyException(String message) {
        super(message);
    }

    public BarleyException(String message, Throwable cause) {
        super(message, cause);
    }

    public BarleyException(Throwable cause) {
        super(cause);
    }
}