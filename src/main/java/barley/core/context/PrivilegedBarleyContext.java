/*
*  Copyright (c) 2005-2011, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
*
*  WSO2 Inc. licenses this file to you under the Apache License,
*  Version 2.0 (the "License"); you may not use this file except
*  in compliance with the License.
*  You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
package barley.core.context;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.description.AxisService;
import org.apache.axis2.engine.AxisConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import barley.core.MultitenantConstants;
import barley.core.internal.BarleyContextDataHolder;
import barley.core.internal.OSGiDataHolder;
import barley.core.utils.ThriftSession;
import barley.core.utils.Utils;
import barley.registry.api.Registry;
import barley.user.api.TenantManager;
import barley.user.api.UserRealm;
import barley.user.api.UserRealmService;
import barley.user.api.UserStoreException;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheManager;

/**
 * This CarbonContext provides users the ability to carry out privileged actions such as
 * switching tenant flows, setting tenant ID etc.
 */
public class PrivilegedBarleyContext extends BarleyContext {

    private static OSGiDataHolder dataHolder = OSGiDataHolder.getInstance();
	private static final Log log = LogFactory.getLog(PrivilegedBarleyContext.class);

    // Private constructor accepting a CarbonContext holder.
    private PrivilegedBarleyContext(BarleyContextDataHolder carbonContextHolder) {
        super(carbonContextHolder);
    }

    /**
     * Starts a tenant flow. This will stack the current CarbonContext and begin a new nested flow
     * which can have an entirely different context. This is ideal for scenarios where multiple
     * super-tenant and sub-tenant phases are required within as a single block of execution.
     *
     * @see BarleyContextDataHolder#startTenantFlow()
     */
    public static void startTenantFlow() {
        Utils.checkSecurity();
        getThreadLocalCarbonContext().getBarleyContextDataHolder().startTenantFlow();
    }

    /**
     * This will end the tenant flow and restore the previous CarbonContext.
     *
     * @see BarleyContextDataHolder#endTenantFlow()
     */
    public static void endTenantFlow() {
        Utils.checkSecurity();
        getThreadLocalCarbonContext().getBarleyContextDataHolder().endTenantFlow();
    }

    public static void unloadTenant(int tenantId) {
    	Utils.checkSecurity();
        BarleyContextDataHolder.unloadTenant(tenantId);
    }

    public static void destroyCurrentContext() {
    	Utils.checkSecurity();
        BarleyContextDataHolder.destroyCurrentCarbonContextHolder();
    }

    /**
     * Obtains the CarbonContext instance stored on the CarbonContext holder.
     *
     * @return the CarbonContext instance.
     */
    public static PrivilegedBarleyContext getCurrentContext() {
    	Utils.checkSecurity();
        return new PrivilegedBarleyContext(null);
    }

    /**
     *
     * @return PrivilegedBarleyContext from the current thread
     */
    public static PrivilegedBarleyContext getThreadLocalCarbonContext(){
    	Utils.checkSecurity();
        return new PrivilegedBarleyContext(BarleyContextDataHolder.getThreadLocalCarbonContextHolder());
    }

    /**
     * Obtains the CarbonContext instance stored on the CarbonContext holder in the given Message
     * Context. If an instance does not exist, it will first be added to the Message Context.
     *
     * @param messageContext The Message Context on which the CarbonContext is found.
     * @return the CarbonContext instance.
     */
    public static PrivilegedBarleyContext getCurrentContext(MessageContext messageContext) {
    	Utils.checkSecurity();
        return new PrivilegedBarleyContext(BarleyContextDataHolder.getCurrentCarbonContextHolder(messageContext));
    }

    /**
     * Obtains the CarbonContext instance stored on the CarbonContext holder in the given Axis2
     * Configuration Context. If an instance does not exist, it will first be added to the
     * Axis2 Configuration Context.
     *
     * @param configContext The Axis2 Configuration Context on which the CarbonContext is found.
     * @return the CarbonContext instance.
     */
    public static PrivilegedBarleyContext getCurrentContext(ConfigurationContext configContext) {
        Utils.checkSecurity();
        return new PrivilegedBarleyContext(BarleyContextDataHolder.getCurrentCarbonContextHolder(configContext));
    }

    /**
     * Obtains the CarbonContext instance stored on the CarbonContext holder in the given Axis2
     * Configuration. If an instance does not exist, it will first be added to the Axis2
     * Configuration.
     *
     * @param axisConfiguration The Axis2 Configuration on which the CarbonContext is found.
     * @return the CarbonContext instance.
     */
    public static PrivilegedBarleyContext getCurrentContext(AxisConfiguration axisConfiguration) {
        Utils.checkSecurity();
        return new PrivilegedBarleyContext(BarleyContextDataHolder.getCurrentCarbonContextHolder(axisConfiguration));
    }

    /**
     * Obtains the CarbonContext instance stored on the CarbonContext holder attached to the given
     * Axis2 Service. If an instance does not exist, it will first be attached to the Axis2 Service.
     *
     * @param axisService The Axis2 Service on which the CarbonContext is attached to.
     * @return the CarbonContext instance.
     */
    public static PrivilegedBarleyContext getCurrentContext(AxisService axisService) {
        Utils.checkSecurity();
        AxisConfiguration axisConfiguration = axisService.getAxisConfiguration();
        return (axisConfiguration != null) ? getCurrentContext(axisConfiguration) :
               getCurrentContext();
    }

    /**
     * Obtains the CarbonContext instance stored on the CarbonContext holder in the given HTTP
     * Session. If an instance does not exist, it will first be added to the HTTP Session
     * Configuration.
     *
     * @param httpSession The HTTP Session on which the CarbonContext is found.
     * @return the CarbonContext instance.
     */
    public static PrivilegedBarleyContext getCurrentContext(HttpSession httpSession) {
        Utils.checkSecurity();
        return new PrivilegedBarleyContext(BarleyContextDataHolder.getCurrentCarbonContextHolder(httpSession));
    }

    /**
     * Obtains the CarbonContext instance stored on the CarbonContext holder in the given Thrift
     * Session. If an instance does not exist, it will first be added to the Thrift Session.
     *
     * @param thriftSession The HTTP Session on which the CarbonContext is found.
     * @return the CarbonContext instance.
     */
    public static PrivilegedBarleyContext getCurrentContext(ThriftSession thriftSession) {
        Utils.checkSecurity();
        PrivilegedBarleyContext PrivilegedBarleyContext =
                new PrivilegedBarleyContext(BarleyContextDataHolder.getCurrentCarbonContextHolder());
        thriftSession.setAttribute("carbonContextHolder",PrivilegedBarleyContext);
        return PrivilegedBarleyContext;
    }

    /**
     * Method to set the tenant id on this CarbonContext instance. This method will not
     * automatically calculate the tenant domain based on the tenant id.
     *
     * @param tenantId the tenant id.
     */
    public void setTenantId(int tenantId) {
        setTenantId(tenantId, false);
    }

    /**
     * Method to set the tenant id on this CarbonContext instance.
     *
     * @param tenantId            the tenant id.
     * @param resolveTenantDomain whether the tenant domain should be calculated based on this
     *                            tenant id.
     */
    public void setTenantId(int tenantId, boolean resolveTenantDomain) {
        getBarleyContextDataHolder().setTenantId(tenantId);
        if (!resolveTenantDomain || tenantId == MultitenantConstants.SUPER_TENANT_ID) {
            return;
        }
        resolveTenantDomain(tenantId);
    }

    /**
     * Method to set the username on this CarbonContext instance.
     *
     * @param username the username.
     */
    public void setUsername(String username) {
        getBarleyContextDataHolder().setUsername(username);
    }

    /**
     * Method to set the tenant domain on this CarbonContext instance. This method will not
     * automatically calculate the tenant id based on the tenant domain.
     *
     * @param tenantDomain the tenant domain.
     */
    public void setTenantDomain(String tenantDomain) {
        setTenantDomain(tenantDomain, false);
    }

    /**
     * Method to set the tenant domain on this CarbonContext instance.
     *
     * @param tenantDomain    the tenant domain.
     * @param resolveTenantId whether the tenant id should be calculated based on this tenant
     *                        domain.
     */
    public void setTenantDomain(String tenantDomain, boolean resolveTenantId) {
        getBarleyContextDataHolder().setTenantDomain(tenantDomain);
        if (!resolveTenantId) {
            return;
        }
        resolveTenantId(tenantDomain);
    }

    /**
     * Method to obtain the tenant domain on this CarbonContext instance. This method can optionally
     * resolve the tenant domain using the tenant id that is already posses.
     *
     * @param resolve whether the tenant domain should be calculated based on the tenant id that is
     *                already known.
     * @return the tenant domain.
     */
    public String getTenantDomain(boolean resolve) {
        if (resolve && getTenantDomain() == null &&
            (getTenantId() > 0 || getTenantId() == MultitenantConstants.SUPER_TENANT_ID)) {
            resolveTenantDomain(getTenantId());
        }
        return getTenantDomain();
    }

    /**
     * Method to obtain the tenant id on this CarbonContext instance. This method can optionally
     * resolve the tenant id using the tenant domain that is already posses.
     *
     * @param resolve whether the tenant id should be calculated based on the tenant domain that is
     *                already known.
     * @return the tenant id.
     */
    public int getTenantId(boolean resolve) {
        if (resolve && getTenantId() == MultitenantConstants.INVALID_TENANT_ID && getTenantDomain() != null) {
            resolveTenantId(getTenantDomain());
        }
        return getTenantId();
    }

    /**
     * Resolve the tenant domain using the tenant id.
     *
     * @param tenantId the tenant id.
     */
    private void resolveTenantDomain(int tenantId) {
        TenantManager tenantManager = getTenantManager();
        if (tenantManager != null) {
            try {
                log.debug("Resolving tenant domain from tenant id");
                setTenantDomain(tenantManager.getDomain(tenantId));
            } catch (UserStoreException ignored) {
                // Exceptions in here, are due to issues with DB Connections. The UM Kernel takes
                // care of logging these exceptions. For us, this is of no importance. This is
                // because we are only attempting to resolve the tenant domain, which might not
                // always be possible.
            }
        }
    }

    /**
     * Resolve the tenant id using the tenant domain.
     *
     * @param tenantDomain the tenant domain.
     */
    private void resolveTenantId(String tenantDomain) {
        TenantManager tenantManager = getTenantManager();
        if (tenantManager != null) {
            try {
                log.debug("Resolving tenant id from tenant domain");
                setTenantId(tenantManager.getTenantId(tenantDomain));
            } catch (UserStoreException ignored) {
                // Exceptions in here, are due to issues with DB Connections. The UM Kernel takes
                // care of logging these exceptions. For us, this is of no importance. This is
                // because we are only attempting to resolve the tenant id, which might not always
                // be possible.
            }
        }
    }

    /**
     * Utility method to obtain the tenant manager from the realm service. This will only work in an
     * OSGi environment.
     *
     * @return tenant manager.
     */
    private TenantManager getTenantManager() {
        try {
        	UserRealmService realmService = dataHolder.getUserRealmService();
            if (realmService != null) {
                return realmService.getTenantManager();
            }
        } catch (Exception ignored) {
            // We don't mind any exception occurring here. Our intention is provide a tenant manager
            // here. It is perfectly valid to not have a tenant manager in some situations.
        }
        return null;
    }

    /**
     * Method to set an instance of a registry on this CarbonContext instance.
     *
     * @param type     the type of registry to set.
     * @param registry the registry instance.
     */
    public void setRegistry(RegistryType type, Registry registry) {
        if (registry != null) {
            BarleyContextDataHolder BarleyContextDataHolder = getBarleyContextDataHolder();
            switch (type) {
                case USER_CONFIGURATION:
                    log.trace("Setting config user registry instance.");
                    BarleyContextDataHolder.setConfigUserRegistry(registry);
                    break;

                case SYSTEM_CONFIGURATION:
                    log.trace("Setting config system registry instance.");
                    BarleyContextDataHolder.setConfigSystemRegistry(registry);
                    break;
                case USER_GOVERNANCE:
                    log.trace("Setting governance user registry instance.");
                    BarleyContextDataHolder.setGovernanceUserRegistry(registry);
                    break;
                case SYSTEM_GOVERNANCE:
                    log.trace("Setting governance system registry instance.");
                    BarleyContextDataHolder.setGovernanceSystemRegistry(registry);
                    break;
                case LOCAL_REPOSITORY:
                    log.trace("Setting local repository instance.");
                    BarleyContextDataHolder.setLocalRepository(registry);
                    break;
            }
        }
    }

    /**
     * {@inheritDoc}
     */    
    public Registry getRegistry(RegistryType type) {
        Registry registry = super.getRegistry(type);
        if (registry != null) {
            return registry;
        }
        switch (type) {
            case SYSTEM_CONFIGURATION:
                try {
                    int tenantId = getTenantId();
                    if (tenantId != MultitenantConstants.INVALID_TENANT_ID) {
                        registry = dataHolder.getRegistryService().getConfigSystemRegistry(tenantId);
                        setRegistry(RegistryType.SYSTEM_CONFIGURATION, registry);
                        return registry;
                    }
                } catch (Exception ignored) {
                    // If we can't obtain an instance of the registry, we'll simply return null. The
                    // errors that lead to this situation will be logged by the Registry Kernel.
                }
                return null;

            case SYSTEM_GOVERNANCE:
                try {
                    int tenantId = getTenantId();
                    if (tenantId != MultitenantConstants.INVALID_TENANT_ID) {
                        registry =
                                dataHolder.getRegistryService().getGovernanceSystemRegistry(
                                        tenantId);
                        setRegistry(RegistryType.SYSTEM_GOVERNANCE, registry);
                        return registry;
                    }
                } catch (Exception ignored) {
                    // If we can't obtain an instance of the registry, we'll simply return null. The
                    // errors that lead to this situation will be logged by the Registry Kernel.
                }
                return null;

            default:
                return null;
        }
    }
    

    /**
     * Method to obtain a named cache instance. Please note that it is highly probable that a named
     * cache may not be pre-initialized like the default cache. When in a cluster, it might take
     * sometime for the cache to join the other members. This would result in cache misses in the
     * meantime.
     *
     * @param name the name of the cache instance.
     * @return the cache instance.
     */
    public Cache getCache(String name) {
        return CacheManager.getInstance().getCache(name);
    }

    public void setUserRealm(UserRealm userRealm) {
        getBarleyContextDataHolder().setUserRealm(userRealm);
    }

    /**
     * Obtain the first OSGi service found for interface or class <code>clazz</code>
     * @param clazz The type of the OSGi service
     * @return The OSGi service
     */
    public Object getOSGiService(Class clazz) {
    	/* (임시주석)
        BundleContext bundleContext = dataHolder.getBundleContext();
        ServiceTracker serviceTracker = new ServiceTracker(bundleContext, clazz, null);
        try {
            serviceTracker.open();
            return serviceTracker.getServices()[0];
        } finally {
            serviceTracker.close();
        }
        */
        return null;
    }

    /**
     * Obtain the OSGi services found for interface or class <code>clazz</code>
     * @param clazz The type of the OSGi service
     * @return The List of OSGi services
     */
    public List<Object> getOSGiServices(Class clazz) {
    	/* (임시주석)
        BundleContext bundleContext = dataHolder.getBundleContext();
        ServiceTracker serviceTracker = new ServiceTracker(bundleContext, clazz, null);
        List<Object> services = new ArrayList<Object>();
        try {
            serviceTracker.open();
            Collections.addAll(services, serviceTracker.getServices());
        } finally {
            serviceTracker.close();
        }
        return services;
        */
    	return null;
    }

    public void setApplicationName(String applicationName) {
        getBarleyContextDataHolder().setApplicationName(applicationName);
    }
}