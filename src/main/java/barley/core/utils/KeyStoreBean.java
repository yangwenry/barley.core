package barley.core.utils;

import java.security.KeyStore;
import java.util.Date;

/**
 * (추가) 2018.01.29 신규추가 
 * @author yangwenry
 *
 */
public class KeyStoreBean {
    private KeyStore keyStore;
    private Date lastModifiedDate;

    public KeyStoreBean(KeyStore keyStore, Date lastModifiedDate) {
        this.keyStore = keyStore;
        this.lastModifiedDate = lastModifiedDate;
    }

    public KeyStore getKeyStore() {
        return keyStore;
    }

    public void setKeyStore(KeyStore keyStore) {
        this.keyStore = keyStore;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}