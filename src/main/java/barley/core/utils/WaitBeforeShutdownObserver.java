package barley.core.utils;

public interface WaitBeforeShutdownObserver {

	/**
     * This method will be invoked on all running components before shutdown happens.
     */
    void startingShutdown();

    /**
     * The server will wait for all tasks to complete before shutting down.
     *
     * @return true if the server needs to wait or false if not.
     */
    boolean isTaskComplete();
}
